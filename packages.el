(defconst jack-packages
  '(flycheck
    (jack-mode :location local)
    ))

(defun jack/init-jack-mode ()
 (use-package jack-mode
    :commands (jack-mode flycheck-jack-setup)
    :defer t
    :mode ("\\.jack\\'" . jack-mode)
    :init (add-hook 'flycheck-mode-hook #'flycheck-jack-setup)))

(defun jack/post-init-flycheck ()
  (spacemacs/enable-flycheck 'jack-mode))
